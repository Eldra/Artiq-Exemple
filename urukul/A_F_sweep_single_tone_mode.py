import sys
sys.path.insert(0, "../config/")
from AD9910_keywords import *

from artiq.experiment import *

class DDS(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.setattr_device("core_dma")
        self.ch = self.get_device("urukul0_ch0")


    @kernel
    def init(self):
        self.core.reset()
        self.ch.cpld.init()
        self.ch.init()
        delay(1*ms)


    @kernel
    def record(self):
        amp_start = 0.5
        amp_end = 1.0
        freq_start = 1*MHz
        freq_end = 10*MHz
        N_step = 100

        A = amp_start
        F = freq_start
        fstep = (freq_end - freq_start)/(N_step-1)
        astep = (amp_end - amp_start)/(N_step-1)
        
        with self.core_dma.record("pulses"):
            for i in range(N_step):
                asf = self.ch.amplitude_to_asf(A)
                ftw = self.ch.frequency_to_ftw(F)
                self.ch.write64(profile0, (asf<<16), ftw)
                self.ch.cpld.io_update.pulse_mu(8)
                A+=astep
                F+=fstep
            self.ch.write64(profile0, (self.ch.amplitude_to_asf(0.)<<16), self.ch.frequency_to_ftw(1*MHz))
            self.ch.cpld.io_update.pulse_mu(8)


 
    @kernel
    def run(self):

        # init
        self.init()

        self.ch.set_att(6.*dB)
        self.ch.sw.on()
        delay(10*ms)

        self.record()
        delay(10*ms)
        pulses_handle = self.core_dma.get_handle("pulses")
        delay(10*ms)
        self.core_dma.playback_handle(pulses_handle)





