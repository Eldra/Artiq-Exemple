import sys
sys.path.insert(0, "../../config/")
from AD9910_keywords import *

from artiq.experiment import *

class DDS(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.channel = self.get_device("urukul0_ch0")
        self.cpld = self.get_device("urukul0_cpld")

    @kernel
    def init_cpld(self):
        self.core.break_realtime()
        self.cpld.init()

    @kernel
    def calibrate_urukul(self):
        self.core.break_realtime()
        self.channel.init()
        self.core.break_realtime()
        sync_delay_seed, _ = self.channel.tune_sync_delay()
        self.core.break_realtime()
        io_update_delay = self.channel.tune_io_update_delay()
        return sync_delay_seed, io_update_delay

    @kernel
    def setup_urukul(self):
        # set frequency /!\ WITH set() FUNCTION NOT set_frequency()
        self.core.break_realtime()
        self.channel.init()
        self.channel.set(50*MHz)
        self.channel.cfg_sw(1)
        self.channel.set_att(6.)
 
    def init_urukul(self):
        self.init_cpld()
        
        # calibrate channel
        eeprom = self.channel.sync_data.eeprom_device
        offset = self.channel.sync_data.eeprom_offset
        sync_delay_seed, io_update_delay = self.calibrate_urukul()
        eeprom_word = (sync_delay_seed >> 24) | (io_update_delay << 16)
        eeprom.write_i32(offset, eeprom_word)

        self.setup_urukul()

    @kernel
    def amp_ramp(self):
        pass

    def run(self):
        self.core.reset()
        self.init_urukul()
