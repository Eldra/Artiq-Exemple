from artiq.experiment import *

class VariableTutorial(EnvExperiment):
    """Variable exemple"""

    def build(self):
        self.setattr_argument("PYONValue", PYONValue())
        self.setattr_argument("BooleanValue", BooleanValue())
        self.setattr_argument("EnumerationValue", EnumerationValue(["tata", "toto", "titi"]))
        self.setattr_argument("NumberValue", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("StringValue", StringValue())

    def run(self):
        print(self.PYONValue)
        print(self.BooleanValue)
        print(self.EnumerationValue)
        print(self.NumberValue)
        print(self.StringValue)
