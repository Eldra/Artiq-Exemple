from artiq.experiment import *

class Zotino(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.Z = self.get_device("zotino0")

    @kernel
    def run(self):
        # init
        self.core.reset()
        self.Z.init()
        delay(20*ms)

        # switch on leds
        self.Z.set_leds(0b00111100)

        # set voltage
        self.Z.write_dac(0, 0.2)
        delay(1*ms)
        
        # load
        self.Z.load()
