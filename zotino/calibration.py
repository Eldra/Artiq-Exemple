from artiq.experiment import *
from time import sleep
from numpy import int32

@portable
def voltage_to_mu(voltage, offset_dacs, vref):
    return int(round(0x10000*(voltage/(4.*vref)) + offset_dacs*0x4))

def show_hex(val):
    print(hex(val))


class Zotino(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.Z = self.get_device("zotino0")
    
    @kernel
    def run(self):
        self.core.reset()
        self.Z.init()
        delay(1*ms)
        self.Z.set_leds(0b00111100)


        # Follow the steps :
        # STEP 0 : init the DAC to default config (offset=0x8000 and gain=0xffff).
        #          Say what is the desire min (vzs) and max (vfs) output voltage.
        #          Say on which channel you want to work.
        #
        # STEP 1 : Set the DAC to the min value. Write the actual output voltage
        #
        # STEP 2 : Set the DAC to the max value. Write the actual output voltage
        #
        # STEP 3 : Configure the channel to be as close as possible of the desire value.
        #          /!\ Only negative offset error are configurable
        #          /!\ Only positive gain error are configurable
        #
        # STEP 4 : Configure test_val as you want to verify the configuration

        STEP = 2

        # STEP = 0
        channel = 16
        vzs_wanted = -9.9
        vfs_wanted = 9.9
        vrange = vfs_wanted - vzs_wanted

        # STEP = 1
        vzs_measured = -9.99

        # STEP = 2
        vfs_measured = 10.016
        
        # STEP = 3
        # calibration

        # STEP = 4
        test_val = 5    # V



        ###########
        # Default #
        ###########
        if STEP == 0:
            self.core.break_realtime()
            self.Z.write_offset_mu(channel, 0x8000)
            self.Z.write_gain_mu(channel, 0xffff)
            delay(1*ms)
            self.Z.load()


        ##############
        # Zero scale #
        ##############
        if STEP == 1:
            self.Z.write_dac_mu(channel, 0x0000)
            delay(1*ms)
            self.Z.load()
        
        ##############
        # Full scale #
        ##############
        elif STEP == 2:
            self.Z.write_dac_mu(channel, 0xffff)
            delay(1*ms)
            self.Z.load()
        
        ################
        # Actual Scale #
        ################
        
        elif STEP == 3:
            # vzs_err and compute the new offset value
            vzs_err = vzs_measured - vzs_wanted
            vzs_lsb_err = round(vzs_err/(vrange/65536))
            print("VZS lsb error :\t", vzs_lsb_err)
            if vzs_err < 0:
                new_offset = 0x8000 + vzs_lsb_err
            else:
                print("vzs_err = ", vzs_err)
                print("Only negative zero scale error can be reduce. Offset = 0x8000 (default)")
                new_offset = 0x8000
             
            # vfs_err and compute the new gain value
            vfs_err = vfs_measured - vfs_wanted
            if vfs_err > 0:
                span_err = vfs_err - vzs_err
                vfs_lsb_err = round(span_err/(vrange/65536))
                print("VFS lsb error :\t", vfs_lsb_err)
                new_gain = 0xffff - vfs_lsb_err
            else:
                print("vfs_err = ", vfs_err)
                print("Only positive full scale error can be reduce. Gain = 0xffff (default)")
                new_gain=0xffff


            # write the new offset and the new gain
            print("New gain :\t", new_gain)
            print("New offset :\t", new_offset)
            self.core.break_realtime()
            self.Z.write_offset_mu(channel, new_offset)
            self.Z.write_gain_mu(channel, new_gain)
            delay(1*ms)
            self.Z.load()


        else:
            self.Z.write_dac(channel, test_val)
            delay(1*ms)
            self.Z.load()
