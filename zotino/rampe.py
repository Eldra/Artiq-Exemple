from artiq.experiment import *

class zotino(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.Z = self.get_device("zotino0")

    @kernel
    def run(self):
        self.core.reset()
        self.Z.init()
        delay(10*ms)

        # switch on leds because it's fun
        self.Z.set_leds(0b00111100)

        S=0.
        E=5.
        N=100
        step=(E-S)/(N-1)
        
        for i in range(N):
            self.Z.write_dac(0, S)
            delay(1*ms)
            self.Z.load()
            S+=step
