from artiq.experiment import *

class ScanTutorial(EnvExperiment):
    """Scan exemple"""

    def build(self):
        self.setattr_argument("NoScan", Scannable(default=NoScan(3, 10)))
        self.setattr_argument("RangeScan", Scannable(default=RangeScan(0, 100, 11)))
        self.setattr_argument("CenterScan", Scannable(default=CenterScan(50, 10, 1)))
        self.setattr_argument("ExplicitScan", Scannable(default=ExplicitScan([1, 4, 5, 6, 9])))

    def run(self):

        print("NoScan :")
        for i in self.NoScan:
            print(i)

        print("RangeScan :")
        for i in self.RangeScan:
            print(i)

        print("CenterScan :")   # /!\ Not linear
        for i in self.CenterScan:
            print(i)

        print("ExplicitScan :")
        for i in self.ExplicitScan:
            print(i)
