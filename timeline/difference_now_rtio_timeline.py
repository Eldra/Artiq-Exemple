from artiq.experiment import *

class Timeline(EnvExperiment):
    def build(self):
        self.setattr_device("core")
    
    @kernel
    def run(self):
        # init
        self.core.reset()
        t0 = now_mu()
    
        print("now :\t", now_mu() - t0)
        print("rtio :\t", self.core.get_rtio_counter_mu() - t0)
        print()
        
        # advance now(). now() becomes higher than rtio_counter()
        delay(1.)
        
        print("now :\t", now_mu() - t0)
        print("rtio :\t", self.core.get_rtio_counter_mu() - t0)
        print()
        
        # wait rtio_counter() >= now()
        self.core.wait_until_mu(now_mu())
        
        print("now :\t", now_mu() - t0)
        print("rtio :\t", self.core.get_rtio_counter_mu() - t0)
