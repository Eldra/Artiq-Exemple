from artiq.experiment import *

class Tutorial(EnvExperiment):

    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl4")
    
    @kernel
    def run(self):
        self.core.reset()
        for i in range(1000000):
            self.ttl4.pulse(100*ns)
            rtio_log("ttl4", "i", i)
            delay(100*ns)
