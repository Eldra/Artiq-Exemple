from artiq.experiment import *
from artiq.coredevice.rtio import rtio_input_timestamp

class Trigger(EnvExperiment):
    def build(self):

        self.setattr_device("core")
        self.setattr_device("core_dma")
        self.LED = self.get_device("led0")
    
    @kernel
    def init(self):
        self.core.reset()
        delay(1*ms)

    @kernel
    def record(self, N):
        with self.core_dma.record("test_dma"):
            for i in range(N):
                self.LED.pulse(200*ms)
                delay(200*ms)

    @kernel
    def run(self):
        self.init()

        self.record(10)
        handle = self.core_dma.get_handle("test_dma")

        self.core.break_realtime()
        
        self.core_dma.playback_handle(handle)
