from artiq.experiment import *
from artiq.coredevice.ad9910 import (RAM_DEST_ASF, RAM_DEST_FTW, RAM_MODE_BIDIR_RAMP, RAM_MODE_RAMPUP)

#This code demonstrates use of the urukul RAM. It produces a 125MHz pulse th
    
RAM_MODE_DIRECTSWITCH = 0
RAM_MODE_RAMPUP = 1
RAM_MODE_BIDIR_RAMP = 2
RAM_MODE_CONT_BIDIR_RAMP = 3
RAM_MODE_CONT_RAMPUP = 4

RAM_DEST_FTW = 0
RAM_DEST_POW = 1
RAM_DEST_ASF = 2
RAM_DEST_POWASF = 3

CFR1 = 0x00


class AD9910RAM(EnvExperiment):
    '''Urukul RAM Amplitude Ramp'''
    def build(self): #this code runs on the host computer
        self.setattr_device("core")
        self.ch = self.get_device("urukul0_ch0")

    def show_hex(self, val):
        print(hex(val & 0xffffffff), " or ", hex(val))


    @kernel
    def init(self):
        self.core.reset()
        self.ch.cpld.init()
        self.ch.init()
        delay(1*ms)
        
    @kernel
    def setup_default(self):
        # Configure the default parameter
        self.ch.set_frequency(30*MHz)
        self.ch.set_amplitude(1.)
        self.ch.cpld.io_update.pulse_mu(8)
        self.ch.set_att(10*dB)
        self.ch.sw.on()

    @kernel
    def safe_write_ram(self, data):
        self.core.break_realtime()
        cfr = self.ch.read32(CFR1)
        delay(1*ms)

        cfr &= 0x7FFFFFFF   # diseable ram
        self.core.break_realtime()
        self.ch.write32(CFR1, cfr) 
        self.ch.cpld.io_update.pulse_mu(8)
        delay(1*ms)
        
        self.core.break_realtime()
        self.ch.write_ram(data)
        delay(10*ms)
        
        cfr |= 0x10000000   # enable ram
        self.core.break_realtime()
        self.ch.write32(CFR1, cfr)
        self.ch.cpld.io_update.pulse_mu(8)
        delay(1*ms)

        

    @kernel #this code runs on the FPGA
    def run(self):

        # create data
        n = 10
        data = [0]*(1<<n)
        for i in range(1024):
            data[i] = self.ch.amplitude_to_asf(i/1023)<<18
 
        self.init()
        
        # set profile
        self.ch.set_profile_ram(start=0, end=1023, step=1, profile=0, mode=RAM_MODE_BIDIR_RAMP)
        self.ch.cpld.set_profile(0)
        self.ch.cpld.io_update.pulse_mu(8)
        delay(1*ms)

        # write ram
        self.safe_write_ram(data)

        # configure ram destination
        self.ch.write32(CFR1, 0xC0000002) # amplitude
        self.ch.cpld.io_update.pulse_mu(8)

        self.setup_default()

        self.core.break_realtime()

        while(1):
            delay(1*ms)
            self.ch.cpld.set_profile(0)
            delay(5*us)
            self.ch.cpld.set_profile(1)
        
