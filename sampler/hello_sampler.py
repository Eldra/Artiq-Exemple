from artiq.experiment import *

# This code uses the channel 25 of the zotino0 board to generate a voltage
# We want to measure this voltage with the sampler0 channel0


class Sampler(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.S = self.get_device("sampler0")
        self.DAC = self.get_device("zotino0")

    @kernel
    def run(self):
        # init
        self.core.reset()
        self.DAC.init()
        self.S.init()
        delay(1*ms)

        # set DAC
        self.DAC.set_leds(0b11000011)
        self.DAC.write_dac(16, 5)
        delay(1*ms)
        self.DAC.load()

        # set sampler

        for i in range(8):
            self.S.set_gain_mu(i, 0)

        delay(100*us)
        smp=[0.0]*8
        self.S.sample(smp)
        print(smp[0])

