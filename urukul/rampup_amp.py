from artiq.experiment import *
from artiq.coredevice.ad9910 import (RAM_DEST_ASF, RAM_DEST_FTW, RAM_MODE_BIDIR_RAMP, RAM_MODE_RAMPUP)

#This code demonstrates use of the urukul RAM. It produces a 125MHz pulse th
    
RAM_MODE_DIRECTSWITCH = 0
RAM_MODE_RAMPUP = 1
RAM_MODE_BIDIR_RAMP = 2
RAM_MODE_CONT_BIDIR_RAMP = 3
RAM_MODE_CONT_RAMPUP = 4

RAM_DEST_FTW = 0
RAM_DEST_POW = 1
RAM_DEST_ASF = 2
RAM_DEST_POWASF = 3

CFR1 = 0x00

FTW_REG = 0x07
POW_REG = 0x08
ASF_REG = 0x09


class AD9910RAM(EnvExperiment):
    '''Urukul RAM Amplitude Ramp'''
    def build(self): #this code runs on the host computer
        self.setattr_device("core")
        self.ch = self.get_device("urukul0_ch0")

    def show_hex(self, val):
        print(hex(val & 0xffffffff))


    @kernel
    def init(self):
        self.core.reset()
        self.ch.cpld.init()
        self.ch.init()
        delay(1*ms)
        
    @kernel
    def setup_default(self):
        # Configure the default parameter
        self.ch.set_frequency(50*MHz)
        self.ch.set_amplitude(1.)
        self.ch.cpld.io_update.pulse_mu(8)
        self.ch.set_att(10*dB)
        self.ch.sw.on()

    @kernel
    def safe_write_ram(self, data):
        self.core.break_realtime()
        cfr = self.ch.read32(CFR1)
        delay(1*ms)

        cfr &= 0x7FFFFFFF   # diseable ram
        self.core.break_realtime()
        self.ch.write32(CFR1, cfr) 
        self.ch.cpld.io_update.pulse_mu(8)
        delay(1*ms)
        
        self.core.break_realtime()
        self.ch.write_ram(data)
        delay(10*ms)
        
        cfr |= 0x10000000   # enable ram
        self.core.break_realtime()
        self.ch.write32(CFR1, cfr)
        self.ch.cpld.io_update.pulse_mu(8)
        delay(1*ms)


    @kernel
    def safe_read_ram(self, size):
        tmp=[0]*size
        for i in range(8):
            self.ch.cpld.set_profile(i)
            self.ch.read_ram(tmp)
            delay(10*ms)
            print(tmp)
        

    @kernel #this code runs on the FPGA
    def run(self):
        self.init()

        # set all profile
        size=10
        for i in range(2):
            self.ch.set_profile_ram(start=i*size, end=(i+1)*size-1, step=10, \
                                    profile=i, mode=RAM_MODE_RAMPUP, nodwell_high=1)
            self.ch.cpld.io_update.pulse_mu(8)


        # write ram for each profile
        for i in range(2):
            self.ch.cpld.set_profile(i)
            self.ch.cpld.io_update.pulse_mu(8)
            data=[0]*size
            for j in range(size):
                print(self.ch.amplitude_to_asf(j/((i+1)*size))<<18)
                data[size-j-1] = self.ch.amplitude_to_asf(j/((i+1)*size))<<18
            self.safe_write_ram(data)

        # set CFR1 in direct switch mode
        self.ch.set_cfr1(ram_destination=RAM_DEST_ASF, ram_enable=1)
        self.ch.cpld.io_update.pulse_mu(8)

        self.setup_default()
        self.core.break_realtime()

        while(1):
            delay(1000*us)
            for i in range(2):
                self.ch.cpld.set_profile(i)
                self.ch.cpld.io_update.pulse_mu(8)
                delay(50*us)
        
