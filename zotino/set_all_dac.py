from artiq.experiment import *

class DAC(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.Z = self.get_device("zotino0")

    @kernel
    def run(self):
        #init
        self.core.reset()
        self.Z.init()
        delay(1*ms)


        values = [5.]*32
        delay(10*ms)
        dacs = [i for i in range(32)]

        self.Z.set_dac(values, dacs)
