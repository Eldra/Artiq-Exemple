# First run this file:
#   artiq-run rtio_analyser.py
#
# Generate the vcd file:
#   artiq_coreanalyzer -w rtio.vcd
#
# Open gtkwave (sudo apt install gtkwave):
#   gtkwave&
#
# Import rtio.vcd:
#   File -> Open New Tab (or just Ctrl + T).
#
# Select a signal with your mouse:
#   On the left (exemple: Type: wire, Signals: rtio_slack[63:0]).
#
# Insert the signal:
#   Very bottom left, clic "Insert".
#
# Now the signal is in the black screen.

# Few command for beginner (like me)
# 1) Zoom / Dezoom : Ctrl + wheel.
# 2) Clic on the black screen to see a cursor.
# 3) Clic on the green arrow to move the cursor on the next event.
# 4) Chose the field to show with the fields "From:[    ] To:[    ]".
# 5) Clic the wheel to add a marker.
# 6) Maintain the left clic and drag the cursor to measure a duration.

from artiq.experiment import *

class Tutorial(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl4")

    @kernel
    def run(self):
        self.core.reset()
        for i in range(100):
            self.ttl4.pulse(100*us)
            rtio_log("ttl4", "i", i)
            delay(100*us)
