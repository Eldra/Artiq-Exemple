import sys
sys.path.insert(0, "../config/")
from AD9910_keywords import *

from artiq.experiment import *

class DDS(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.channel = self.get_device("urukul0_ch0")
        self.switch = self.get_device("ttl_urukul0_sw0")
        self.cpld = self.get_device("urukul0_cpld")


 
    @kernel
    def init_urukul(self):
        self.core.break_realtime()
        self.cpld.init()
        self.channel.init()

        self.channel.set(50*MHz)
        self.channel.cfg_sw(0)
        
    @kernel
    def rf_switch(self):
        self.core.break_realtime()
        self.channel.cfg_sw(0)

        for i in range(20): 
            t = now_mu() - self.core.seconds_to_mu(0.2)
            while self.core.get_rtio_counter_mu() < t:
                pass
            self.switch.pulse(10*us)
            delay(10*us)


    def run(self):
        self.init_urukul()
        self.rf_switch()
