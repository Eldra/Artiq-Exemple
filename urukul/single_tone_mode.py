import sys
sys.path.insert(0, "../config/")
from AD9910_keywords import *

from artiq.experiment import *

class DDS(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.ch = self.get_device("urukul0_ch0")


    ###################
    # Utils functions #
    ###################

    def show_hex(self, cfr1):
        print(hex(cfr1 & 0xffffffff))
 
    @kernel
    def init(self):
        self.core.reset()
        self.ch.cpld.init()
        self.ch.init()
        delay(1*ms)
 
    @kernel
    def run(self):
        self.init()
        ftw = self.ch.frequency_to_ftw(50*MHz)
        asf = self.ch.amplitude_to_asf(1.)

        self.ch.write64(profile0, (asf<<16), ftw)
        self.ch.cpld.io_update.pulse_mu(8)
        self.ch.set_att(0*dB)
        self.ch.sw.on()


